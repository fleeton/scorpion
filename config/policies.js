/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  HomeController: {
    'logout': ['isLoggedIn'],
    't1': ['isLoggedIn', 'isCustomer']
  },

  CustomerController: {
    'signup': ['notLoggedIn'],
    'signin': ['notLoggedIn'],
    'self': ['isLoggedIn'],
    'update': ['isLoggedIn'],
    'logout': ['isLoggedIn', 'isCustomer'],
    '*': false
  },

  SupplierController: {
    'signup': ['notLoggedIn'],
    'signin': ['notLoggedIn'],
    'self': ['isLoggedIn', 'isSupplier'],
    'update': ['isLoggedIn', 'isSupplier'],
    'logout': ['isLoggedIn', 'isSupplier'],
    '*': false
  },

  CityController: {
    '*': true
  },

  RouteController: {
    'add': ['isLoggedIn', 'isSupplier'],
    'find': true,
    '*': false
  },

  QuotesController: {
    'get': ['isLoggedIn'],
    'deny': ['isLoggedIn', 'isSupplier'],
    'accept': ['isLoggedIn', 'isSupplier'],
    '*': false
  },

  BookingController: {
    'create': ['isLoggedIn', 'isCustomer'],
    'update': ['isLoggedIn', 'isCustomer'],
    'get': ['isLoggedIn', 'isCustomer'],
    '*': false
  }
};
