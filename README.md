# SCORPION 
```sh
Vengeance will be mine!
```

### About me
```sh
Hanzo Hasashi, now known as Scorpion (全蠍人, "Full Scorpion Man"), is a resurrected ninja in the Mortal Kombat fighting game series. He is one of the very few original characters, debuting in the first Mortal Kombat arcade game. He holds the distinction, along with Raiden and Sub-Zero (in one form or another), of appearing in every generation of Mortal Kombat games as a playable character.
```

### Lil bit more
```sh
It is known that his father, a former member of the Shirai Ryu, forbade his son from joining the clan, as he did not wish for his son to live the life of an assassin. However, Hanzo joined in spite of his father's wishes in order to provide his wife and son with a comfortable life.

Now Scorpion is a hell-spawned spectre, inexorably seeking vengeance against those responsible for the destruction of his clan and the death of his own family. Despite his malevolent appearance, he is not inherently evil. He joins the forces of evil when promised a means of resurrecting his clan on Earth, or the chance to inflict his wrath against those who butchered them. Scorpion has also (indirectly) assisted the game's protagonists to fulfill these motives.

Scorpion is perceived by fans as the title's foremost anti-hero. He undertakes actions that benefit the forces of good, albeit in his own gruesome and vigilante manner. His attitude, appearance and self-reliance have all contributed to his popularity. Compared to the purely virtuous "good guys", and the diabolically evil "bad guys", Scorpion's moral neutrality is unique. Although he is featured alongside the forces of evil in the opening scene of Mortal Kombat: Armageddon, he remains neutral because of his fierce hatred of Quan Chi.

Scorpion appears human when masked, though this is merely an illusion. Only his skull remains when in his true form, sometimes ablaze. However, he can fully regain his human appearance if he conquers his own demons. Scorpion's most popular and recognized skill is his famous spear attack, which is used to draw an opponent toward him. This attack will also stun the opponent for a short amount of time. Scorpion will shout, "Get over here!" or "Come here!", while executing this technique.

Although Scorpion is often strict and wrathful, at times he shows a caring side and is very honorable. In the original timeline, when he discovers the Sub-Zero in the second tournament isn't Bi-Han and in fact his more merciful brother, Scorpion vows to protect Kuai Liang instead for killing his kin. In the second timeline, Scorpion truly intended to spare the first Sub-Zero in exchange for the resurrection of his clan, but Quan Chi prevented this. Most notably, in the Mortal Kombat X Comic, it's shown he took in victims and survivors of the Netherrealm War into his iteration of the Shirai Ryu to now protect Earthrealm while also raising Takeda Takahashi. During this time, he treated Takeda like his own son, such as when he comforts Takeda after Fox's death.
```

### Appearance
```sh
Scorpion appeared as a yellow palette swap of Sub-Zero. He kept this appearance from the first MK to MK4 after which he bore two swords on his back and his kunai attached to a rope tied to his belt. He has white eyes with his mask on. Without his mask, his head is a (sometimes flaming) skull. In MK4, he is further distinguished by his skeletal motif, mostly in the mask, with bone-like structures lacing his uniform. As of MKX, he has regained his humanity and human identity, depicted with a goatee and mustache and can switch between his human and familiar spectre-like appearance at will.

Scorpion unmasked is known as Inferno Scorpion. This design reappears in the Challenge Tower, fighting Kano in the Netherrealm. His costumes incorporate his namesake more and more with each game, especially his latest redesign. The hilts of his swords now resemble the stingers of scorpions, while his shoulder pads and mouthpiece are also molded after scorpions.

Scorpion's yellow costume is said to have mocked not only Sub-Zero, but also the Lin Kuei, as Takeda (who was a member of the Lin Kuei) developed Ninjutsu, which he considered a superior fighting style to what the Lin Kuei had. He quickly left the Lin Kuei and formed the Shirai Ryu clan, the Lin Kuei's main enemies.
```

### Combat Characteristics
```sh
Scorpion is most commonly associated with hellfire, the Netherrealm's variant of fire. Scorpion is immune to the element and primarily uses it to confirm the death of his opponents, spewing it from his skull while unmasked. As a spectre, Scorpion is immune to death as his soul is still bound by revenge, allowing him to endlessly chase his targets until they have been silenced. He has the ability to teleport, often used in the form of surprise or ambush attacks. The scope of Scorpion's powers depend on how long he remains in his abode, the Netherrealm. This proved advantageous when he pursued Quan Chi in the depths of hell, whose magic is diminished by the power and nature of the realm.

After being fully resurrected, it's revealed in Mortal Kombat X (Comic Series) that Scorpion's wraith powers and hellfire depend on his own emotional pain, as he must relive his greatest shame (the destruction of his family and clan) or witness his allies suffering (Takeda seemingly dying) in order to use it. By doing so, Hanzo becomes more wraith like, surrounded by hellfire, and his powers increase tremendously to the point they can even exceed deities in combat like Raiden. Although powerful, there is a drawback to every time Hanzo uses his powers; if he uses hellfire for too long, he risks being consumed by it, destroying his existence. Along with this, Hanzo's Scorpion persona can overtake him, resulting in him acting less reasonable and more hostile.

Like many ninjas, Scorpion is well-versed in the art of armed kombat. He has wielded various weapons throughout the tournaments, from axes to the most recent twin Ninjato. His most recurring weapon is the Spear, a kunai attached to a sturdy rope, representing Scorpion's "Stinger". At times, the spear is empowered with hellfire for more power. Various depictions of the spear had been made before Deadly Alliance. It was shown as a chain tied to a mace in the comics. His spear was a sentient, serpentine creature that spawned from within his hand during the films. He is empowered by the Elder Gods to defeat Onaga in Deception. However, Shujinko slays Onaga before he gets the chance.

Scorpion carries two ninja swords. From MKDA to MKA he only uses one of them as his weapon style, but in MK 2011 he finally uses both of them on some of his moves. Scorpion's fighting stance, from the original MK to MK Gold, originates from the martial art of Shaolin Fist; his right arm emulates a "scorpion tail". In MKX he is capable of summoning a fire minion to assist him if using the Inferno character variation. He is also capable of hurling fireballs if using the Hellfire character variation.
```
