/**
 * QuotesController
 *
 * @description :: Server-side controller logic for managing Quotes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    //Get the quotes
    //Can be done by supplier as well as customer
    //filters - status!
	get: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var limit = req.param('limit');
    if(!limit)
        limit = 10;
    else{
        try{
            limit = parseInt(limit);
        }
        catch (e){
            return res.send({errorMessage: "The limit must be an integer!", errorCode: "VALIDATION_ERROR"}, 400);
        }
    }

    var page = req.param('page');
    if(!page)
        page = 1;
    else{
        try{
            page = parseInt(page);
        }
        catch (e){
            return res.send({errorMessage: "page must be interger value!", errorCode: "VALIDATION_ERROR"}, 400);
        }
    }

    var status = req.param('status');
    if(!status)
        status = 0;

    else{
        try{
            status = parseInt(status);
        }
        catch (e){
            return res.send({errorMessage: "The status must be interger!", errorCode: "VALIDATION_ERROR"}, 400);
        }
    }

    if(req.session.userType == "supplier"){
        var supplierId = req.session.userId;

        QuoteService.findQuote(supplierId, limit, page, status, function(err, quote){
            if(err){
                return res.send(err, 400);
            }

            return res.send(quote, 200);
        });
    } 

    else if(req.session.userType == "customer"){
        var customerId = req.session.userId;

        var bookingId = req.param('bookingId');
        if(!bookingId)
            return res.send({errorMessage: "booking id is required!", errorCode: "VALIDATION_ERROR"}, 400);

        var quoteId = req.param('id');

        QuoteService.findQuoteForCustomer(customerId, bookingId, quoteId, limit, page, status, function(err, quote){
            if(err){
                return res.send(err, 400);
            }

            return res.send(quote, 200);
        });
    }

    else
        return  res.send({errorMessage: "Unknown user!", errorCode: "LOGIC_ERROR"}, 400);
    },

    //denying a quote!
    //Can be done by supplier!
    deny: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var quoteId = req.param('id');
    if(!quoteId)
        return res.send({errorMessage: "The quote is not defined!", errorCode: "VALIDATION_ERROR"}, 400);

    var supplierId = req.session.userId;

    QuoteService.denyQuote(supplierId, quoteId, function(err, deniedQuote){
        if(err)
            return res.send(err, 400);
        else
            return res.send(deniedQuote, 200);
    });
    },

    //Accepting a quote
    //can be done by a suuplier!
    accept: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var quoteId = req.param('id');
    if(!quoteId)
        return res.send({errorMessage: "The quote is not defined!", errorCode: "VALIDATION_ERROR"}, 400);

    var dateOfAvailability = req.param('delivery_date');
    if(!dateOfAvailability)
        return res.send({errorMessage: "The date of delivery as not been defined!", errorCode: "VALIDATION_ERROR"}, 400);

    var priceRaised = req.param('price');
    if(!priceRaised)
        return res.send({errorMessage: "The cost of delivery has not been defined!", errorCode: "VALIDATION_ERROR"}, 400);

    var tracking = req.param('tracking');
    if(!tracking)
        tracking = false;
    else{
        try{
            tracking = parseInt(tracking);
            if(tracking > 0)
                tracking = true;
            else
                tracking = false;
        }
        catch (e){
            return res.send({errorMessage: "Unidentified value of tracking!", errorCode: "VALIDATION_ERROR"}, 400);
        }
    }

    var supplierId = req.session.userId;

    QuoteService.acceptDeal(supplierId, quoteId, dateOfAvailability, priceRaised, tracking, function(err, acceptedDeal){
        if(err)
            return res.send(err, 400);

        return res.send(acceptedDeal, 200);
    });
    },

    //TODO: Archive a quote
    //To be done by a supplier!
    archive: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var quoteId = req.param('id');
    if(!quoteId)
        return res.send({errorMessage: "The quote is not defined!", errorCode: "VALIDATION_ERROR"}, 400);

    var supplierId = req.session.userId;

    QuoteService.archiveQuote(supplierId, quoteId, function(err, deniedQuote){
        if(err)
            return res.send(err, 400);
        else
            return res.send(deniedQuote, 200);
    });
    }
};

