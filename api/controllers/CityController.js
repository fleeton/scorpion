/**
 * CityController
 *
 * @description :: Server-side logic for managing Cities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
    //Should be called, when a supplier adds a route!
    //Ideally should be accessed by anyone!
    //TODO: The Ids should go only to the logged in user!
    startswith: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var name = req.param('starts_with');
    if(!name){
        return res.send({errorMessage: "No parameter caught", errorCode: "VALIDATION_ERROR"}, 400);
    }
    CityService.getSimilarCity(name, function(err, cities){
        if(err){
            return res.send(err, 400);
        }

        return res.send(cities, 200);
    });
    }
};

