/**
 * CustomerController
 *
 * @description :: Server-side logic for managing Customers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	signup: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    // var username = req.param('username');
    var password = req.param('password');
    var firstname = req.param('firstname');
    var lastname = req.param('lastname');
    var email = req.param('email');
    var phone = req.param('phone');
    
    // if(!username){
    //     return res.send({errorMessage: "username missing", errorCode: "VALIDATION_ERROR"}, 400);
    // }

    if(!password){
        return res.send({errorMessage: "password missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!firstname){
        return res.send({errorMessage: "firstname is missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!lastname){
        return res.send({errorMessage: "lastname is missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!email){
        return res.send({errorMessage: "emial missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!phone){
        return res.send({errorMessage: "phone missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    // LoginService.createCustomer(username, password, firstname, lastname, email, phone, function(err, supplier){
    LoginService.createCustomer(password, firstname, lastname, email, phone, function(err, customer){
        if(err){
            sails.log.debug(err);
            req.session.errorMessage = err;
            return res.send({status: 403}, 400);
        }

        req.errorMessage = "successfully signed up!";
        return res.send({status: 200}, 200);
        // return res.send(customer, 200);
    });
    },

    signin: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var email = req.param('email');
    var password = req.param('password');

    if(!email || !password){
        return res.send({errorMessage: "Credentials missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    LoginService.customerEntry(email, password, function(err, detail){
        if(err){
            sails.log.debug(err);
            req.session.errorMessage = err.errorMessage;
            return res.send({status: 403}, 400);
        }

        req.session.authenticated = true;
        req.session.userId = detail.id;
        req.session.userType = "customer";
        req.session.fname = detail.firstname;
        req.session.lname = detail.lastname;
        req.session.errorMessage = null;

        TokenService.addNewToken(detail.id, 1, "customer", req.sessionID, function(err, session){
            if(err)
                sails.log.error("Customer loggedin! but the session could not be added! The customer id is : "+detail.id);
            else
                sails.log.info("New customer loggedin, session added successfully");
        });

        return res.send({status: 200}, 200);
        // return res.redirect('/');
    });
    },

    self: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var userId = req.session.userId;
    UserService.getCustomerSelf(userId, function(err, supplier){
        if(err){
            return res.send(err, 400);
        }

        return res.send(supplier, 200);
    });
    },

    update: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var userId = req.session.userId;
    var fname = req.param('firstname');
    var lname = req.param('lastname');
    // var email = req.param('email');
    var phone = req.param('phone');

    // UserService.updateCustomer(userId, fname, lname, email, phone, function(err, updatedResult){
    UserService.updateCustomer(userId, fname, lname, phone, function(err, updatedResult){
        if(err){
            return res.send(err, 400);
        }

        return res.send(updatedResult, null);
    });
    },

    logout: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var userId = req.session.userId;
    var sessionId = req.sessionID;

    TokenService.destroySession(userId, 1, req.sessionID, function(err, destroyedSession){
        if(err)
            sails.log.error("The session id could not be deleted!");
        else
            sails.log.info("User successfully logged out!");
    });

    req.session.destroy();
    return res.redirect('/');
    }
};

