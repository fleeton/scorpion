/**
 * SupplierController
 *
 * @description :: Server-side logic for managing Suppliers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	

    signup: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var password = req.param('password');
    var name = req.param('name');
    var email = req.param('email');
    var phone = req.param('phone');
    

    if(!password){
        return res.send({errorMessage: "password missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!name){
        return res.send({errorMessage: "You can't be nameless", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!email){
        return res.send({errorMessage: "emial missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!phone){
        return res.send({errorMessage: "phone missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    LoginService.createSupplier(password, name, email, phone, function(err, supplier){
        if(err){
            return res.send(err, 400);
        }

        return res.send(supplier, 200);
    });
    },

    signin: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var email = req.param('email');
    var password = req.param('password');

    if(!email || !password){
        return res.send({errorMessage: "Credentials missing", errorCode: "VALIDATION_ERROR"}, 400);
    }

    LoginService.supplierEntry(email, password, function(err, detail){
        if(err){
            return res.send(err, 400);
        }

        req.session.authenticated = true;
        req.session.userId = detail.id;
        req.session.userType = "supplier";
        req.session.fname = detail.name;
        req.session.lname = null;

        TokenService.addNewToken(detail.id, 1, "supplier", req.sessionID, function(err, session){
            if(err)
                sails.log.error("Supplier loggedin! but the session could not be added! The supplier id is : "+detail.id);
            else
                sails.log.info("New supplier loggedin, session added successfully");
        });
        
        return res.send(detail, 200);
    });
    },

    self: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var userId = req.session.userId;
    UserService.getSupplierSelf(userId, function(err, supplier){
        if(err){
            return res.send(err, 400);
        }

        return res.send(supplier, 200);
    });
    },

    update: function(req, res){

    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var userId = req.session.userId;
    var name = req.param('name');
    var email = req.param('email');
    var phone = req.param('phone');
    var description = req.param('description');
    var website = req.param('website');

    UserService.updateSupplier(userId, name, email, phone, description, website, function(err, updatedResult){
        if(err){
            return res.send(err, 400);
        }

        return res.send(updatedResult, null);
    });
    },

    logout: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var userId = req.session.userId;
    var sessionId = req.sessionID;

    TokenService.destroySession(userId, 1, req.sessionID, function(err, destroyedSession){
        if(err)
            sails.log.error("The session id could not be deleted!");
        else
            sails.log.info("User successfully logged out!");
    });

    req.session.destroy();
    return res.redirect('/');
    }
};

