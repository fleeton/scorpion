/**
 * RouteController
 *
 * @description :: Server-side logic for managing Routes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
    //The routes here will be added by supplier.
    add: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var name = req.param('name');
    var sourceId = req.param('source');
    var destId = req.param('dest');
    var supplierId = req.session.userId;
    var intermediates = req.param("intermediates");

    if(!sourceId){
        return res.send({errorMessage: "Source not defined!", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!destId){
        return res.send({errorMessage: "Desination not defined!", errorCode: "VALIDATION_ERROR"}, 400);
    }

    RouteService.addRoute(supplierId, name, sourceId, destId, intermediates, function(err, route){
        if(err){
            return res.send(err, 400);
        }

        return res.send(route, 200);
    });
    },

    //Finding suppliers fot the route!
    //Should this be here or should stay in SupplierController?
    //Can only be run by a Customer!
    find: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var source = req.param('from');
    var dest = req.param('to');

    if(!source){
        return res.send({errorMessage: "The source has not been defined!", errorCode: "VALIDATION_ERROR"}, 400);
    }

    if(!dest){
        return res.send({errorMessage: "The destination has not been defined!", errorCode: "VALIDATION_ERROR"}, 400);
    }

    RouteService.findTotalSuppliers(source, dest, function(err, routes){
        /* Total suppliers who operate in those routes. They might have more than one vehicle */
        if(err){
            return res.send(err, 400);
        }

        return res.send(routes, 200);
    });
    }
};

