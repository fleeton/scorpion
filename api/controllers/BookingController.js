/**
 * BookingController
 *
 * @description :: Server-side logic for managing Bookings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    //Creating a booking! Can only be done by a customer!
	create: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var source = req.param('from');
    var dest = req.param('to');

    //TODO :: Validate source and destination.
    var doj = req.param('doj');
    //TODO :: Parse the date of journey.
    var qed = req.param('quote');
    //TODO :: Parse the quotation ending date.

    var category = req.param('category');
    var weight = req.param('weight');
    var volume = req.param('volume');
    var fraction = req.param('fraction');
    var description = req.param('description');

    if(!source)
        return res.send({errorMessage: "Source is miising!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!dest)
        return res.send({errorMessage: "Destination is missing!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!doj)
        return res.send({errorMessage: "Date of Journey is missing!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!qed)
        return res.send({errorMessage: "Quotation ending date is not defined!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!category)
        return res.send({errorMessage: "The goods category is mendatory!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!weight)
        return res.send({errorMessage: "Weight is missing!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!volume)
        return res.send({errorMessage: "Volume is missing!", errorCode: "VALIDATION_ERROR"}, 400);
    if(!fraction)
        return res.send({errorMessage: "Fraction is missing!", errorCode: "VALIDATION_ERROR"}, 400);


    var customerId = req.session.userId;
    BookingService.createNewBooking(source, dest, doj, qed, category, weight, volume, fraction, description, customerId, function(err, booking){
        if(err){
            return res.send(err, 400);
        }

        return res.send(booking, 200);
    });
    },

    //customer uses this endpoint to cancel or close the booking!
    //Cancelling - All the quotes will be set to cStatus == cancelled
    //Close - All the Quotes will be set to cStatus == closed
    //In both of the cases suppliers won't be able to modify the quotes.
    update: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    var bookingId = req.param('id');
    var status = req.param('status');

    if(!bookingId)
        return res.send({errorMessage: "The booking not defined", errorCode: "VALIDATION_ERROR"}, 400);
    if(!status)
        return res.send({errorMessage: "No data to update", errorCode: "VALIDATION_ERROR"}, 400);
    if(status == "pending" || status == "confirmed")
        return res.send({errorMessage: "You are not authorize to do that!", errorCode: "VALIDATION_ERROR"}, 403);

    var customerId = req.session.userId;
    BookingService.update(customerId, bookingId, status, function(err, updatedBooking){
        if(err)
            return res.send(err, 400);
        else
            return res.send(updatedBooking, 200);
    });
    },


    //Customer uses this endpoint to get all of his/her bookings.
    //The filter is only the status of booking!
    get: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );
    
    var limit = req.param('limit');
    if(!limit)
        limit = 10;
    else{
        try{
            limit = parseInt(limit);
        }
        catch (e){
            return res.send({errorMessage: "The limit must be an integer!", errorCode: "VALIDATION_ERROR"}, 400);
        }
    }

    var page = req.param('page');
    if(!page)
        page = 1;
    else{
        try{
            page = parseInt(page);
        }
        catch (e){
            return res.send({errorMessage: "page must be interger value!", errorCode: "VALIDATION_ERROR"}, 400);
        }
    }

    var status = req.param('status');
    if(!status)
        status = "pending";
    var customerId = req.session.userId;

    BookingService.getBookings(customerId, limit, page, status, function(err, bookins){
        if(err)
            return res.send(err, 400);
        else
            return res.send(bookins, 200);
    });
    }
};
