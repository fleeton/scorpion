/**
 * HomeController
 *
 * @description :: Server-side logic for managing homes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	

    home: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    return res.view('index', {"session": req.session, layout: null});
    },

    login: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    return res.view('login', {layout: null});
    },

    logout: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    if(req.userType == 'supplier')
        return res.redirect('/supplier/logout');
    else
        return res.redirect('/customer/logout');
    },

    auth: function(req, res){
        return res.redirect('/');
    },
    
    t1: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    if(req.headers.referer != 'http://localhost:1337/')
        return res.send({errorMessage: "Cross site not allowed!"}, 403);

    var from = req.param('from');
    var to = req.param('to');
    //TODO :: Recheck the suppliers!
    //Give error message if no supplier found!
    return res.view('index-04', {layout: null, 'to': to, 'from': from});
    },

    t2: function(req, res){
    sails.log.debug("TIME: "+ new Date()+ ": "
        +req.method+ " " +req.headers.host+" " +req.url+ " "
        +JSON.stringify(req.params)+" "+req.route.params
    );

    return res.view('index-05');
    }
};

