exports.createNewBooking = function(source, dest, doj, qed, category, weight, volume, fraction, description, customerId, cb){

    var directRouteSupplier, returnRouteSupplier, sourceFormatted, destFormatted;

    RouteService.getTotalSupplierList(source, dest, function(err, supplierDetail){
        if(err)
            return cb(err, null);

        directRouteSupplier = supplierDetail.direct;
        returnRouteSupplier = supplierDetail.noDirect;
        sourceFormatted = supplierDetail.sForAdd;
        destFormatted = supplierDetail.dForAdd;

        if(directRouteSupplier <= 0 && returnRouteSupplier <= 0)
            return cb({errorMessage: "Unfortunately we do not operate in those routes! But we'll get back to it soon! Thank you!", errorCode: "ROUTE_UNAVAILABLE"}, null);

        Booking.create({source: sourceFormatted, dest: destFormatted, dateOfJourney: doj, quoteEndingDate: qed, category: category, weight: weight, volume: volume, fraction: fraction, description: description, customerId: customerId, noOfSupplierInformed: directRouteSupplier.length+returnRouteSupplier.length}).exec(function(err, booking){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            var totalDirect = 0;
            totalDirect = directRouteSupplier.length;

            var totalReturn = 0;
            totalReturn = returnRouteSupplier.length;
            
            for(var i=0; i<totalDirect; i++){
                (function(supplier){
                    Quotes.create({bookingId: booking.id, supplierId: supplier, expiryDate: qed}).exec(function(err, sup){
                        if(err){
                            sails.log.error(err);
                        }
                    });
                })(directRouteSupplier[i]);
            }

            for(var i=0; i<totalReturn; i++){
                (function(supplier){
                    Quotes.create({bookingId: booking.id, supplierId: supplier, expiryDate: qed}).exec(function(err, sup){
                        if(err){
                            sails.log.error(err);
                        }
                    });
                })(returnRouteSupplier[i]);
            }

            return cb(null, booking);
        });
    });
    
}

exports.update = function(customerId, bookingId, status, cb){
    Booking.findOne({id: bookingId}).exec(function(err, booking){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!booking)
            return cb({errorMessage: "No such type of booking exists!", errorCode: "BOOKING_NOT_FOUND"}, null);

        if(booking.customerId != customerId)
            return cb({errorMessage: "This booking does not belong to you!", errorCode: "ACCESS_DENIED"}, null);

        Booking.update({id: booking.id}, {status: status}).exec(function(err, updatedBooking){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            //TODO :: Populate the Quotes with same, i.e. the status data!

            else
                return cb(null, updatedBooking);
        });
    });
}

exports.getBookings = function(customerId, limit, page, status, cb){
    Booking.find({ where: {customerId: customerId, status: status}, sort: 'createdAt DESC'}).paginate({page: page, limit: limit}).exec(function(err, bookings){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        return cb(null, bookings);
    });
}