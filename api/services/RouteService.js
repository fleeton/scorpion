var request = require('superagent');
var prefix = require('superagent-prefix')('/static');

exports.addRoute = function(supplierId, name, sourceId, destId, intermediates, cb){
    City.findOne({id: sourceId}).exec(function(err, source){
        if(err){
            sails.log.error(err);
            return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
        }

        if(!source){
            sails.log.info(sourceId);
            return cb({errorMessage: "Obsolate source", errorCode: "VALIDATION_ERROR"}, null);
        }

        City.findOne({id: destId}).exec(function(err, dest){
            if(err){
                sails.log.error(err);
                return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
            }

            if(!dest){
                return cb({errorMessage: "Obsolate dest", errorCode: "VALIDATION_ERROR"}, null);
            }

            // sails.log.info(dest);
            //TODO: Implement a two step commit here. Add transaction support.
            //Source :- https://docs.mongodb.org/manual/tutorial/perform-two-phase-commits/
            Route.create({supplierId: supplierId, name: name, source: source.coordinate, dest: dest.coordinate, sCityId: source.id, dCityId: dest.id}).exec(function(err, route){
                if(err){
                    sails.log.error(err);
                    return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
                }

                Source.create({supplierId: supplierId, coordinate: source.coordinate, routeId: route.id}).exec(function(err, sourceRoute){
                    if(err){
                        sails.log.error("Critical: Source not created! "+route.id);
                        sails.log.error(err);
                    }
                });

                Dest.create({supplierId: supplierId, coordinate: dest.coordinate, routeId: route.id}).exec(function(err, destRoute){
                    if(err){
                        sails.log.error("Critical: Dest not created! "+route.id);
                        sails.log.error(err);
                    }   
                });
                return cb(null, route);
            })
        })
    });
}

exports.findDirectRoute = function(source, dest, cb){
    var sourceRouteIds = [];
    var supplierIds = [];
    Source.native(function(err, collection){
        collection.aggregate([
            {
                '$geoNear': {
                    near: { type: "point", coordinates: source },
                    distanceField: "dist.calculated",
                    maxDistance: 70000,
                    includeLocs: "dist.location",
                    num: 50,
                    spherical: true
                }
            },

            {
                $project: {
                    _id: 1,
                    routeId: 1
                }
            }
        ]).toArray(function(err, directRoute){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            for(var i=0; i<directRoute.length; i++)
                sourceRouteIds.push(directRoute[i].routeId)

            Dest.native(function(err, collection){
                collection.aggregate([
                    {
                        $geoNear: {
                            near: { type: "point", coordinates: dest },
                            distanceField: "dist.calculated",
                            maxDistance: 70000,
                            includeLocs: "dist.location",
                            query: {routeId: {$in: sourceRouteIds}},
                            num: 50,
                            spherical: true
                        }
                    },

                    {
                        $project: {
                            supplierId: 1
                        }
                    }
                ]).toArray(function(err, supplierIdsJSON){
                    if(err){
                        sails.log.error(err);
                        return cb(err);
                    }

                    for(var i=0; i<supplierIdsJSON.length; i++)
                        supplierIds.push(supplierIdsJSON[i].supplierId);

                    return cb(null, supplierIds);
                });
            });
        });
    });
}


exports.findReturnRoutes = function(source, dest, cb){
    var sourceRouteIds = [];
    var supplierIds = [];
    Source.native(function(err, collection){
        collection.aggregate([
            {
                '$geoNear': {
                    near: { type: "point", coordinates: dest },
                    distanceField: "dist.calculated",
                    maxDistance: 70000,
                    includeLocs: "dist.location",
                    num: 50,
                    spherical: true
                }
            },

            {
                $project: {
                    _id: 1,
                    routeId: 1
                }
            }
        ]).toArray(function(err, returnRoute){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            for(var i=0; i<returnRoute.length; i++)
                sourceRouteIds.push(returnRoute[i].routeId)

            Dest.native(function(err, collection){
                collection.aggregate([
                    {
                        $geoNear: {
                            near: { type: "point", coordinates: source },
                            distanceField: "dist.calculated",
                            maxDistance: 70000,
                            includeLocs: "dist.location",
                            query: {routeId: {$in: sourceRouteIds}},
                            num: 50,
                            spherical: true
                        }
                    },

                    {
                        $project: {
                            supplierId: 1
                        }
                    }
                ]).toArray(function(err, supplierIdsJSON){
                    if(err){
                        sails.log.error(err);
                        return cb(err);
                    }

                    for(var i=0; i<supplierIdsJSON.length; i++)
                        supplierIds.push(supplierIdsJSON[i].supplierId);

                    return cb(null, supplierIds);
                });
            });
        });
    });
}

exports.findTotalSuppliers = function(source, dest, cb){
 
     //validate coordinate points
    coordinatesExtract(source, dest, function(err, cdnts){
        if(err)
            return cb({errorMessage: err, errorCode: "No Internet Motherfucker!"}, null);

        source = cdnts[0];
        dest = cdnts[1];

        RouteService.findReturnRoutes(source, dest, function(err, returnRoute){
            if(err)
                return cb(err, null);

            RouteService.findDirectRoute(source, dest, function(err, directRoute){
                if(err)
                    return cb(err, null);

                var message = {
                    directRouteSupplier : directRoute.length,
                    returnRouteSupplier : returnRoute.length
                };

                return cb(null, message);
            });
        });
    });
}

exports.getTotalSupplierList = function(source, dest, cb){

    coordinatesExtract(source, dest, function(err, cdnts){
        if(err)
            return cb({errorMessage: err, errorCode: "No Internet Motherfucker!"}, null);

        source = cdnts[0];
        dest = cdnts[1];

        sForAdd = cdnts[2];
        dForAdd = cdnts[3];

        RouteService.findReturnRoutes(source, dest, function(err, returnRoute){
            if(err)
                return cb(err, null);

            RouteService.findDirectRoute(source, dest, function(err, directRoute){
                if(err)
                    return cb(err, null);

                var message = {
                    direct : directRoute,
                    noDirect : returnRoute,
                    sForAdd : sForAdd,
                    dForAdd : dForAdd
                };
                
                return cb(null, message);
            });
        });
    });
}

/* Fix this function */
//it crashes the server in case a wrong source and dest input is encountered
//as the json it caught after request is not serializable.

//PS :: Find a better method to find coordinates along with formatted_address!!
coordinatesExtract = function(source, dest, cb){
    var sCor, dCor;
    var sForAdd, dForAdd;

    request
    .get('http://maps.google.com/maps/api/geocode/json?address='+source+'&sensor=false')
    .use(prefix) // Prefixes *only* this request
    .end(function(err, res){
        try{
            sCor = JSON.parse(res.text);
        }
        catch (e){
            sails.log.error(e);
            return cb("The source cannot be geocoded!", null);
        }

        sForAdd = sCor['results'][0]['formatted_address'];
        sCor = sCor['results'][0]['geometry']['location'];

        request
        .get('http://maps.google.com/maps/api/geocode/json?address='+dest+'&sensor=false')
        .use(prefix) // Prefixes *only* this request
        .end(function(err, res){

            if(err)
                return cb(err, null);
            try{
                dCor = JSON.parse(res.text);
            }
            catch (e){
                return cb("The destination cannot be geocoded!", null);
            }
            dForAdd = dCor['results'][0]['formatted_address'];
            dCor = dCor['results'][0]['geometry']['location'];
            var fuck_it = [[sCor['lng'], sCor['lat']], [dCor['lng'], dCor['lat']], sForAdd, dForAdd];
            return cb(null, fuck_it);
        });
    });
}
