var varSuppPendingQuote = 0;
var varSuppAgreeQuote = 1;
var varSuppArchiveQuote = 2;
var varSuppDenyQuote = 3;

var varCustPendingQuote = 0;
var varCustAgreeQuote = 1;
var varCustArchiveQuote = 2;
var varCustCancelQuote = 3;
var varCustDeleteQuote = 4;
var varCustAgreeOtherSuppQuote = 5;
var varCustInterestedQuote = 6;
var varCustRejectQuote = 7;

exports.findQuote = function(supplierId, limit, page, status, cb){
    Quotes.find({ where: {supplierId: supplierId, status: status}, select: ['id', 'bookingId', 'supplierId', 'expiryDate', 'status', 'fraction', 'tracking'], sort: 'createdAt DESC'}).paginate({page: page, limit: limit}).exec(function(err, quotes){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        var x = 0;
        var xy = quotes;
        for(var i=0; i<quotes.length; i++){
            (function(bookingId, index){
                Booking.findOne({id: bookingId}).exec(function(err, booking){
                    if(err){
                        xy[index].booking = {};
                        sails.log.error(err);
                        x += 1;
                    }

                    else{
                        delete booking.customerId;
                        delete booking.createdAt;
                        delete booking.updatedAt;
                        delete booking.noOfSupplierInformed;
                        xy[index].booking = booking;
                        x += 1;
                    }

                    if(x == quotes.length)
                        cb(null, xy);
                });
            })(quotes[i].bookingId, i)
        }
    });
}

exports.findQuoteForCustomer = function(customerId, bookingId, quoteId, limit, page, status, cb){

    Booking.findOne({id: bookingId}).exec(function(err, booking){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!booking)
            return cb({errorMessage: "No such kind of booking is present!", errorCode: "BOOKING_NOT_FOUND"}, null);

        if(booking.customerId != customerId)
            return cb({errorMessage: "This booking does not belong to you!", errorCode: "ACCESS_DENIED"}, null);

        if(quoteId){
            Quotes.findOne({id: quoteId}).exec(function(err, quote){
                if(err){
                    sails.log.error(err);
                    return cb(err, null);
                }

                //the quote has not been found! fake quoteId
                if(!quote)
                    return cb({errorMessage: "This quote does not exists!", errorCode: "QUOTE_NOT_FOUND"}, null);

                //supplier has not accepted this quote!
                if(quote.status != 1)
                    return cb({errorMessage: "You are not allowed to see this!", errorCode: "ACCESS_DENIED"}, null);

                if(quote.bookingId != booking.id)
                    return cb({errorMessage: "This quote does not belong to this booking!", errorCode: "QUOTE_REFEREMCING_ERROR"}, null);

                return cb(null, quote);
            });
        }

        else{
            Quotes.find({ where: {bookingId: bookingId, cStatus: status, status: 1}, sort: 'createdAt DESC'}).paginate({page: page, limit: limit}).exec(function(err, quotes){
                if(err){
                    sails.log.error(err);
                    return cb(err, null);
                }

                return cb(null, quotes);
            });
        }
    });
    
}

exports.denyQuote = function(supplierId, quoteId, cb){
    Quotes.findOne({id: quoteId}).exec(function(err, quote){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!quote){
            return cb({errorMessage: "Unknown data requested!", errorCode: "QUOTE_NOT_FOUND"}, null);
        }

        if(quote.supplierId != supplierId)
            return cb({errorMessage: "This quote does not belong to you!", errorCode: "ACCESS_DENIED"}, null);

        Quotes.update({id: quote.id}, {status: varSuppDenyQuote}).exec(function(err, deniedQuote){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            return cb(null, deniedQuote);
        });
    });
}

exports.archiveQuote = function(supplierId, quoteId, cb){
    Quotes.findOne({id: quoteId}).exec(function(err, quote){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!quote){
            return cb({errorMessage: "Unknown data requested!", errorCode: "QUOTE_NOT_FOUND"}, null);
        }

        if(quote.supplierId != supplierId)
            return cb({errorMessage: "This quote does not belong to you!", errorCode: "ACCESS_DENIED"}, null);

        Quotes.update({id: quote.id}, {status: varSuppArchiveQuote}).exec(function(err, archivedQuote){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            return cb(null, archivedQuote);
        });
    });
}

exports.acceptDeal = function(supplierId, quoteId, dateOfAvailability, priceRaised, tracking, cb){
    Quotes.findOne({id: quoteId}).exec(function(err, quote){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!quote){
            return cb({errorMessage: "Unknown data requested!", errorCode: "QUOTE_NOT_FOUND"}, null);
        }

        if(quote.supplierId != supplierId)
            return cb({errorMessage: "This quote does not belong to you!", errorCode: "ACCESS_DENIED"}, null);

        // sails.log.info(quote.expiryDate);
        if(quote.expiryDate < new Date())
            return cb({errorMessage: "This quote has been expired!", errorCode: "TIME_EXPIRED"}, null);

        Booking.findOne({id: quote.bookingId}).exec(function(err, booking){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            if(booking.status != "pending")
                return cb({errorMessage: "The booking to this quote has already confirmed!", errorCode: "BOOKING_CONFIRMED"}, 400);

            Quotes.update({id: quote.id}, {dateOfAvailability: dateOfAvailability, priceRaised: priceRaised, tracking: tracking, status: varSuppAgreeQuote}).exec(function(err, acceptedQuote){
                if(err){
                    sails.log.error(err);
                    return cb(err, null);
                }

                return cb(null, acceptedQuote);
            });
        });
    });
}
