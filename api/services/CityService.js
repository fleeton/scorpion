exports.getSimilarCity = function(name, cb){
    City.find({name: new RegExp(name, "i")}).exec(function(err, cities){
        if(err){
            sails.log.error(err);
            return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
        }

        return cb(null, cities);
    });
}