
exports.addNewToken = function(userId, isWeb, userType, sessionId, cb){
    /*
     * Ques : What happans to the tokens which get deleted because of age?
     * Ans : Don;t know! Should be the work of CronJob!
     * Concern : The token will be present in mongo, but not in redis! 
     * Another solution : Restart redis every month, flush this model every month. +1
     */
    Token.findOne({userId: userId}).exec(function(err, theToken){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        var query;
        if(!theToken){
            if(isWeb == 1)
                query = {userId: userId, userType: userType, mobileTokens: [], webSessionIds: [sessionId]};
            else
                query = {userId: userId, userType: userType, mobileTokens: [sessionId], webSessionIds: []};

            Token.create(query).exec(function(err, newToken){
                if(err){
                    sails.log.error(err);
                    return cb(err, null);
                }

                return cb(null, newToken);
            });
        }

        else{
            var oldMobTokens = theToken.mobileTokens;
            var oldWebSessions = theToken.webSessionIds;

            if(isWeb == 1){
                oldWebSessions.push(sessionId);
                query = {webSessionIds: oldWebSessions};
            }
            else{
                oldMobTokens.push(sessionId);
                query = {mobileTokens: oldMobTokens};
            }
            /*
             * We might have better query than this!
             * TODO :: Refer the docs.
             */

            Token.update({id: theToken.id}, query).exec(function(err, updatedToken){
                if(err){
                    sails.log.error(err);
                    return cb(err, null);
                }

                return cb(null, updatedToken);
            });
        }
    });
}

exports.destroySession = function(userId, isWeb, sessionId, cb){
    Token.findOne({userId: userId}).exec(function(err, tokenDetail){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!tokenDetail){
            sails.log.error("CRITICAL :: Defaq this happaned! This should be impossible to occure!");
            return cb(1, null);
        }

        var query, tokenArray, index;
        if(isWeb == 1){
            tokenArray = tokenDetail.webSessionIds;
            index = tokenArray.indexOf(sessionId);
            if(index > -1)
                tokenArray.splice(index, 1);
            else{
                sails.log.error("This token was not found!");
                return cb(null, 1);
            }
            
            query = {webSessionIds: tokenArray};
        }
        else {
            tokenArray = tokenDetail.mobileTokens;
            index = tokenArray.indexOf(sessionId);
            if(index > -1)
                tokenArray.splice(index, 1);
            else{
                sails.log.error("This token was not found!");
                return cb(null, 1);
            }
            
            query = {mobileTokens: tokenArray};
        }

        Token.update({id: tokenDetail.id}, query).exec(function(err, updatedToken){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            return cb(null, updatedToken);
        });
    });
}
