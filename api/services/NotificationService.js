var mailer = require('mailer');
var username = sails.config.custom.MandrillUsername;
var password = sails.config.custom.MandrillApiKey;

exports.sendMail = function(mes, cb){
    mailer.send({host: "smtp.mandrillapp.com", 
        port: 587, 
        to: "nkmann2@gmail.com",
        from: "me@maruti3pl.co.in",
        subject: "Yo bro the app has been crashed!",
        body: mes,
        authentication: "login",
        username: username,
        password: password
    }, function(err, result){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        return cb(null, result);
    });
}

exports.emailConfirmation = function(email, hash, cb){
    var message = "Confirm your email by clicking to this link http://localhost:1337/user/email/"+hash;
    mailer.send({host: "smtp.mandrillapp.com", 
        port: 587, 
        to: email,
        from: "help@maruti3pl.co.in",
        subject: "Email confirmation",
        body: message,
        authentication: "login",
        username: username,
        password: password
    }, function(err, result){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        return cb(null, result);
    });
}

exports.sendConfirmationCode = function(phone, token, cb){
    /* The phone messages */
}