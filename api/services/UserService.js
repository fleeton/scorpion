exports.getSupplierSelf = function(userId, cb){
    Supplier.findOne({id: userId}).exec(function(err, supplier){
        if(err){
            sails.log.error(err);
            return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
        }

        if(!supplier){
            sails.log.error("Defaq just happaned? This error is impossible");
            return ("error", null);
        }

        else{
            delete supplier.createdAt;
            delete supplier.updatedAt;
            delete supplier.password;
            return cb(null, supplier);
        }
    })
}

exports.getCustomerSelf = function(userId, cb){
    Customer.findOne({id: userId}).exec(function(err, customer){
        if(err){
            sails.log.error(err);
            return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
        }

        if(!customer)
            return cb("Unsupported error", null);

        else{
            delete customer.createdAt;
            delete customer.updatedAt;
            delete customer.password;
            return cb(null, customer);
        }
    })
}

exports.updateSupplier = function(userId, name, email, phone, description, website, cb){
    Supplier.findOne({id: userId}).exec(function(err, supplier){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!supplier){
            return cb("Unsupported error", null);
        }

        if(email != supplier.email){
            if(phone != supplier.phone){
                var query = {name: name, email: email, phone: phone, description: description, website: website, phoneVerified: false, emailVerified: false};
                /* send phone activation code */
            }

            else{
                var query = {name: name, email: email, description: description, website: website, emailVerified: false};
                /* send email verification link */
            }
        }

        else{
            var query = {name: name, description: description, website: website};
        }

        Supplier.update({id: supplier.id}, query).exec(function(err, updatedSupplier){
            if(err){
                sails.log.error(err);
                return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
            }

            else{
                delete updatedSupplier[0].password;
                return cb(null, updatedSupplier[0]);
            }
        });
    });
}

// exports.updateCustomer = function(userId, fname, lname, email, phone, cb){
/* Cannot update email */
exports.updateCustomer = function(userId, fname, lname, phone, cb){
    Customer.findOne({id: userId}).exec(function(err, customer){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!customer){
            return cb("Unsupported error", null);
        }

        if(phone != customer.phone){
            var query = {firstname: fname, lastname: lname, phone: phone, phoneVerified: false};
        }

        else{
            var query = {firstname: fname, lastname: lname};
        }

        Customer.update({id: customer.id}, query).exec(function(err, updateCustomer){
            if(err){
                sails.log.error(err);
                return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
            }

            else{
                delete updateCustomer[0].password;
                return cb(null, updateCustomer[0]);
            }
        });
    });
}
