var bcrypt = require("bcrypt");
var crypto = require('crypto');

exports.createSupplier = function(password, name, email, phone, cb){
    /* username, email, phone are unique field */
    Supplier.findOne({'or': [{ email: email }, { phone: phone} ]}).exec(function(err, supplier){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(supplier != null){
            if(supplier.email == email)
                return cb({errorMessage: "This email is already taken", errorCode: "VALIDATION_ERROR"}, null);
            else if(supplier.phone == phone)
                return cb({errorMessage: "Phone numver already registered", errorCode: "VALIDATION_ERROR"}, null);
        }

        Supplier.create({password: password, name: name, email: email, phone: phone}).exec(function(err, supplier){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            crypto.randomBytes(16, function(err, buf) {
                if(err){
                    sails.log.error(err);
                    return cb(err, null, null, null);
                }

                var token = buf.toString('hex');
                Confirmation.create({userId: supplier.id, utility: 0, code: token}).exec(function(err, conf){
                    if(err){
                        sails.log.info(err);
                    }
                    else {
                        NotificationService.emailConfirmation(email, token, function(err, notice){});
                    }
                });
            });

            crypto.randomBytes(3, function(err, buf) {
                if(err){
                    sails.log.error(err);
                    return cb(err, null, null, null);
                }

                var token = buf.toString('hex');
                Confirmation.create({userId: supplier.id, utility: 1, code: token, isCustomer: false}).exec(function(err, conf){
                    if(err){
                        sails.log.info(err);
                    }
                    else {
                        NotificationService.sendConfirmationCode(phone, token, function(err, notice){});
                    }
                });
            });

            return cb(null, supplier);
        });
    });
}

// exports.createCustomer = function(username, password, firstname, lastname, email, phone, cb){
exports.createCustomer = function(password, firstname, lastname, email, phone, cb){
    /* username, email, phone are unique field */
    // Customer.findOne({'or': [ { username: username }, { email: email }, { phone: phone} ]}).exec(function(err, customer){
    Customer.findOne({'or': [{ email: email }, { phone: phone} ]}).exec(function(err, customer){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(customer != null){
            // if(customer.username == username)
            //     return cb({errorMessage: "This username already exists", errorCode: "VALIDATION_ERROR"}, null);
            if(customer.email == email)
                return cb({errorMessage: "This email is already taken", errorCode: "VALIDATION_ERROR"}, null);
            else if(customer.phone == phone)
                return cb({errorMessage: "Phone numver already registered", errorCode: "VALIDATION_ERROR"}, null);
        }

        // Customer.create({username: username, password: password, firstname: firstname, lastname: lastname, email: email, phone: phone}).exec(function(err, customer){
        Customer.create({email: email, phone: phone, password: password, firstname: firstname, lastname: lastname}).exec(function(err, customer){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            crypto.randomBytes(16, function(err, buf) {
                if(err){
                    sails.log.error(err);
                    return cb(err, null, null, null);
                }

                var token = buf.toString('hex');
                Confirmation.create({userId: customer.id, utility: 0, code: token, isCustomer: true}).exec(function(err, conf){
                    if(err){
                        sails.log.info(err);
                    }
                    else {
                        NotificationService.emailConfirmation(email, token, function(err, notice){});
                    }
                });
            });

            return cb(null, customer);
        });
    });
}

exports.customerEntry = function(email, password, cb){
    Customer.findOne({email: email}).exec(function(err, customer){
        if(err){
            sails.log.error(err);
            return cb(err, null);
        }

        if(!customer){
            return cb({errorMessage: "No such email exists", errorCode: "NO_SUCH_USER"}, null);
        }

        var hash = customer.password;
        bcrypt.compare(password, hash, function(err, result){
            if(err){
                sails.log.error(err);
                return cb(err, null);
            }

            if(!result){
                return cb({errorMessage: "Wrong password", errorCode: "ACCESS_DENIED"}, null);
            }

            else {
                delete customer.createdAt;
                delete customer.updatedAt;
                delete customer.password;
                return cb(null, customer);
            }
        });
    });
}

exports.supplierEntry = function(email, password, cb){
    Supplier.findOne({email: email}).exec(function(err, supplier){
        if(err){
            sails.log.error(err);
            return cb({errorMessage: err, errorCode: "DATABASE_ERROR"}, null);
        }

        if(!supplier){
            return cb({errorMessage: "No such email", errorCode: "NO_SUCH_USER"}, null);
        }

        var hash = supplier.password;
        bcrypt.compare(password, hash, function(err, result){
            if(err){
                sails.log.error(err);
                return cb({errorMessage: err, errorCode: "BCRYPT_ERROR"}, null);
            }

            if(!result){
                return cb({errorMessage: "Wrong password", errorCode: "ACCESS_DENIED"}, null);
            }

            else {
                delete supplier.createdAt;
                delete supplier.updatedAt;
                delete supplier.password;
                return cb(null, supplier);
            }
        });
    });
}
