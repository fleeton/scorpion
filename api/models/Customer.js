/**
* Customer.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {

  connection: 'user',

  attributes: {
    firstname: {
        type: 'STRING',
        maxLength: 50,
        required: true
    },

    lastname: {
        type: 'STRING',
        maxLength: 50,
        required: true
    },

    email: {
        type: 'EMAIL',
        maxLength: 50,
        required: true,
        unique: true
    },

    phone: {
        type: 'STRING',
        required: true,
        unique: true
    },

    password: {
        type: 'STRING',
        minLength: 4,
        columnName: 'encrypted_password'
    },

    isActive: {
        type: 'BOOLEAN',
        defaultsTo: true
    },

    isSpammer: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    isBlocked: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    description: {
        type: 'STRING',
        maxLength: 500
    },

    phoneVerified: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    emailVerified: {
        type: 'BOOLEAN',
        defaultsTo: false
    }

  },

    beforeCreate: function(values, next) {
        bcrypt.hash(values.password, 10, function(err, hash) {
            if(err) return next(err);

            values.password = hash;
            // values.username = values.username.toString().toLowerCase();

            next();
        });
    },
};

