/**
* Quotes.js
*
* @description :: TODO: With more suppliers, there will be more quotes, which will degrade the performance.
               :: We might wanna make another database, with separate quotes for each supplier as a model.
* @docs        :: http://sailsjs.org/#!documentation/models
* THis model is write extensive.
* It contains the 
* enum: ['pending', 'confirmed', 'cancelled', 'deleted']
* The status definition :-
    0 -> Pending, just started.
    1 -> Supplier agreed,
    2 -> Supplier archived,
    3 -> Supplier Denied
* The cStatus definition :-
    Full - Customer status.
    0 -> pending, just started,
    1 -> agreed with this supplier,
    2 -> archive this supplier,
    3 -> cancelled this booking,
    4 -> deleted this booking,
    5 -> agreed with other suppliers,
    6 -> customer is interested in this quote.
*/

module.exports = {

  attributes: {
    connection: 'transport', //<- To be changed later

    bookingId: {
        type: 'STRING',
        required: true
    },

    supplierId: {
        type: 'STRING',
        required: true
    },

    dateOfAvailability: {
        type: 'DATE'
    },

    priceRaised: {
        type: 'INTEGER'
    },

    status: {
        type: 'INTEGER',
        defaultsTo: 0
    },

    cStatus: {
        type: 'INTEGER',
        defaultsTo: 0
    },

    expiryDate:{
        type: 'DATE',
        required: true
    },

    fraction: {
        // 0 -> Half truck
        // 1 -> Full truck
        type: 'INTEGER',
        defaultsTo: 1
    },

    tracking: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    message: {
        type: 'STRING',
        maxLength: 200
    }
  }
};

