/**
* Token.js
*
* @description :: Contains the detail of tokens and cookies, by which a user has logged in!.
* @description :: Contains the list of ips a user used to access this.
* @docs        :: http://sailsjs.org/#!documentation/models
* @TODO        :: Map session and IP! so that we can say - You are logged in from the ip : this, like google does!
* @Doubt       :: Can the userIds of customer and supplier may collide?
*/

module.exports = {

  connection: 'user',
  attributes: {
    userId: {
        type: 'STRING',
        maxLength: 32,
        required: true,
        unique: true
    },

    userType: {
        type: 'STRING',
        enum: ['customer', 'supplier']
    },

    mobileTokens: {
        type: 'ARRAY'
    },

    webSessionIds: {
        type: 'ARRAY'
    },

    ips: {
        //defined here, but will be touched later!
        type: 'ARRAY'
    }
  }
};

