/**
* Booking.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  
  connection: 'transport',

  attributes: {
    source: {
        type: 'STRING',
        maxLength: 32,
        required: true
    },

    dest: {
        type: 'STRING',
        maxLength: 32,
        required: true
    },

    dateOfJourney: {
        type: 'DATE',
        required: true
    },

    quoteEndingDate: {
        type: 'DATE',
        required: true
    },

    supplierId: {
        type: 'STRING',
        minLength: 32
    },

    customerId: {
        type: 'STRING',
        maxLength: 32,
        required: true
    },

    category: {
        //Modify this to category id.
        type: 'STRING'
    },

    weight: {
        type: 'INTEGER'
        //in TONS
    },

    volume: {
        type: 'INTEGER',
        //in Cubik Mts
    },

    deal: {
        type: 'INTEGER',
        defaultsTo: 0,
        required: true
    },

    status: {
        type: 'STRING',
        defaultsTo: "pending",
        enum: ['pending', 'confirmed', 'cancelled', 'deleted']
    },

    trackingId: {
        type: 'STRING'
    },

    isTrackable: {
        type: 'BOOLEAN',
        defaultsTo: false
    },
    
    noOfSupplierInformed: {
        type: 'INTEGER',
        defaultsTo: 0
    },

    fraction:{
        type: 'FLOAT',
        //is half? == 0.5
        //if full == 1
    },

    description: {
        type: 'STRING',
        maxLength: 200
    }
  }
};

