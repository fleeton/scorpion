/**
* Dest.js
*
* @description :: This model contains the geo indexed destination.
* db.route.createIndex( { coordinate : "2dsphere" } )
* GeoNear can be used here!
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    supplierId: {
        type: 'STRING',
        required: true
    },

    routeId: {
        type: 'STRING',
        required: true
    },

    coordinate: {
        /* Destination City Coordinates -- Indexed -- Helps in Searching O(Logn) */
        type: 'ARRAY',
        required: true
    }
  }
};

