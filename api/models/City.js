/**
* City.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  connection: 'transport',

  attributes: {
    name: {
        type: 'STRING',
        required: true
    },

    coordinate: {
        type: 'ARRAY',
        required: true
    }
  },

  beforeCreate: function(values, next) {
    var c = values.coordinate;
    values.coordinate = [parseFloat(c[0]), parseFloat(c[1])];
    next();      
  },
};

