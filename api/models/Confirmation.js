/**
* Confirmation.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* utility = 0 for email verification, 
* utility = 1 for phone verification, 
* utility = 2 for password resetting via email, 
* Not for now - utility = 3 for password resetting via mobile phone app!
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
 
  connection: 'user',

  attributes: {
    
    userId:{
        type: 'STRING',
        required: true
    },

    utility:{
        type: 'INTEGER',
        required: true
    },

    code:{
        type: 'STRING',
        maxLength: 32,
        minLength: 6
    },

    isCustomer: {
        type: 'BOOLEAN'
    }
  }
};