/**
* Route.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
* This model is not indexed!
* The geoNear command and the $geoNear pipeline stage require that a collection have at most only one 2dsphere index and/or only one 2d index whereas geospatial query operators (e.g. $near and $geoWithin) permit collections to have multiple geospatial indexes.
* source - https://docs.mongodb.org/manual/tutorial/build-a-2dsphere-index/
*/

module.exports = {

  connection: 'transport',

  attributes: {
    supplierId: {
        type: 'STRING',
        required: true
    },
    
    name: {
        type: 'STRING',
        required: true
    },

    source: {
        type: 'ARRAY',
        required: true
    },

    dest: {
        type: 'ARRAY',
        required: true
    },

    sCityId: {
        /* Source city id */
        type: 'STRING',
        required: true
    },

    dCityId: {
        /* dest city id */
        type: 'STRING',
        required: true
    },

    i1:{
        type: 'ARRAY'
    },
    i2:{
        type: 'ARRAY'
    },
    i3:{
        type: 'ARRAY'
    },

    other: {
        type: 'ARRAY'
    }
  }
};

