/**
* Source.js
*
* @description :: This model contains the geoindexed Source!
* db.route.createIndex( { coordinate : "2dsphere" })
* GeoNear queries can be used here!
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    supplierId: {
        type: 'STRING',
        required: true
    },

    routeId: {
        type: 'STRING',
        required: true
    },

    coordinate: {
        /* Source City Coordinates -- Indexed -- Helps in Searching O(Logn)*/
        type: 'ARRAY',
        required: true
    }
  }
};

