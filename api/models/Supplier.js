/**
* Supplier.js
*
* @description :: the supplier model
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {

  connection: 'user',
  
  attributes: {
    name: {
        type: 'STRING',
        required: true
    },

    email: {
        type: 'EMAIL',
        required: true,
        unique: true
    },

    password: {
        type: 'STRING',
        minLength: 4,
        columnName: 'encrypted_password'
    },

    vehicles: {
        type: 'INTEGER',
        defaultsTo: 0
    },

    isActive: {
        type: 'BOOLEAN',
        defaultsTo: true
    },

    isSpammer: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    isBlocked: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    description: {
        type: 'STRING',
        maxLength: 500
    },

    phone: {
        type: 'STRING',
        required: true
    },

    phoneVerified: {
        type: 'BOOLEAN',
        defaultsTo: false
    },

    emailVerified: {
        type: 'STRING',
        defaultsTo: false
    },

    website: {
        type: 'STRING',
        maxLength: 100
    },

  },

    beforeCreate: function(values, next) {
        bcrypt.hash(values.password, 10, function(err, hash) {
            if(err) return next(err);

            values.password = hash;

            next();
        });
    },
};

