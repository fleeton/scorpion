/**
 * isSupplier
 *
 * @module      :: Policy
 * @description :: Ensures user is the supplier`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

  if (req.session.userType == "supplier") {
    return next();
  }

  return res.forbidden('You are not allowed to do that.');
};
