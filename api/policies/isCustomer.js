/**
 * isCustomer
 *
 * @module      :: Policy
 * @description :: Ensures user is the customer`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

  if (req.session.userType == "customer") {
    return next();
  }

  return res.forbidden('You are not allowed to do that.');
};
