function signup(){
    $('.login-form')[0].style.visibility = "hidden";
    $('.signup-form')[0].style.visibility = "visible";
}

function login(){
    $('.signup-form')[0].style.visibility = "hidden";
    $('.login-form')[0].style.visibility = "visible";
}

function signupFormSubmit(){
    $('#customer-signup-form')[0].submit();
    parent.checkLogin();
}

function loginFormSubmit(){
    $('#customer-login-form')[0].submit();
    parent.checkLogin();
    // top.window.location.replace("/");
}