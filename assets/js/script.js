$(document).ready(function() {
    $('select').material_select();
    $('.modal-trigger').leanModal();
});


function initializeAutocomplete() {
    // DOM elements representing the source/destination input
    var srcInput = document.getElementById('from');
    var destInput = document.getElementById('to');

    // set country (India) specific bounds to the results 
    var boundOptions = {
        componentRestrictions: {country: 'in'}
    };

    // autocomplete objects with provided bounds
    var srcAutocomplete = new google.maps.places.Autocomplete(srcInput, boundOptions);
    var destAutocomplete = new google.maps.places.Autocomplete(destInput, boundOptions);

    // listen to 'place_changed' events in source field
    google.maps.event.addListener(srcAutocomplete, 'place_changed', function() {
        var place = srcAutocomplete.getPlace();
        console.log(place);
        if (!place.geometry) {
          return;
        }
    });

    // listen to 'place_changed' events in destination field
    google.maps.event.addListener(destAutocomplete, 'place_changed', function() {
        var place = destAutocomplete.getPlace();
        console.log(place);
        if (!place.geometry) {
          return;
        }
    });
}
