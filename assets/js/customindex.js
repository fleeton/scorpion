var count = 1;

function addOneTd(){
    console.log(count);
    count += 1;

    var txt = `
        <tr class="dynam-cell">
        <td><a href="#" onclick="addOneTd()"><img class="responsive-img load-icons" src="/images/icons/Website-06-06-14.png"></td></a>
        
        <td class="item-type-ds">
            <input type="text" id="item-type-`+count+`" class="validate">
            <label for="text" data-error="wrong" data-success="right"></label>
        </td>
        <td class="item-ton-ds">
            <input type="text" id="item-ton-`+count+`" class="validate">
            <label for="number" data-error="wrong" data-success="right"></label>
        </td>
        <td class="item-ton-text">
            <span>Tons</span>
        </td>
        <td class="item-kg-ds">
            <input type="text" id="item-kg-`+count+`" class="validate">
            <label for="number" data-error="wrong" data-success="right"></label>
        </td>
        <td class="item-ton-text">
            <span>Kgs</span>
        </td>
        </tr>
    `;

    $(txt).insertAfter($(".dynam-cell").last());

}

function submit(){
    var to = $('#to')[0].value;
    var from = $('#from')[0].value;
    console.log(to);

    if(parseInt(to) == 0 || parseInt(from) == 0 || to == from){
            Materialize.toast('Failed! Recheck the Source and Destination', 4000);
            return;
        }

    var email = prompt("Please enter your email");
    if (email != null) {


        Materialize.toast('Thank you! We will get to you soon!', 2000);

        $.ajax({
          type: "POST",
          url: "http://transport.co.in:5000",
          data: "to="+to+"&from="+from+"&email="+email,
        });

        window.setTimeout(function(){
            window.location.href = "http://transport.co.in";
        }, 2000);
    }
    else
    Materialize.toast('Failed!', 4000);
}

function send_mofo_toast(){
    Materialize.toast('We are working on it!', 1000);
}

function hhide(){
    $('.login-panel')[0].style.visibility = "visible";
    $('.login-panel')[0].style.backgroundColor =  "rgba(71, 17, 17, 0.75)";
}

function unhhide(){
    $('.login-panel')[0].style.visibility = "hidden";
}

function checkLogin(){
    var iFrame = $('#login-screen')[0];

    iFrame.onload = function () {
        console.log(iFrame.contentDocument.body.innerHTML);
        window.location.replace("/");
    };
}

function getSupplier(){
    var from = $('#from')[0].value;
    var to = $('#to')[0].value;
    var email = $('#email')[0].value;

    var data = {
        from: from,
        to: to,
        email: email
    };

    if (validateEntryPoints()){
        $.ajax({
            type: "POST",
            url: '/route/find',
            data: data,
            success: function(data){
                // alert("There are total "+data.directRouteSupplier+" direct route suppliers and "+data.returnRouteSupplier+" return route suppliers are available, Login to continue booking them!");
                $('#directR').text(data.directRouteSupplier);
                $('#returnR').text(data.returnRouteSupplier);
                $('#directRP')[0].style.visibility = "visible";
                $('#returnRP')[0].style.visibility = "visible";
                $('#errorM')[0].style.visibility = "hidden";
                $('#supavai1')[0].click();
            },

            error: function(data){
                $('#directRP')[0].style.visibility = "hidden";
                $('#returnRP')[0].style.visibility = "hidden";
                $('#errorM')[0].style.visibility = "visible";
                $('#errorM').text(data.responseText);
                $('#supavai1')[0].click();
            },

            dataType: 'json'
        });
    }
    else{
        Materialize.toast('Failed! Recheck the Source and Destination', 4000);
        return;
    }
}

function proceed(){
    var from = $('#from')[0].value;
    var to = $('#to')[0].value;

    if (validateEntryPoints()){
        $('#order-form')[0].submit();
        return;
    }
    else{
        Materialize.toast('Failed! Recheck the Source and Destination', 4000);
        return;
    }
}

function validateEntryPoints(){
    var from = $('#from')[0].value;
    var to = $('#to')[0].value;

    if(from == "" || to == "" || from == to)
        return 0;
    else
        return 1;
}